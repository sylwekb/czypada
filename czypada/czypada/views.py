from django.conf import settings
from rest_framework.views import APIView, Response
from rest_framework.exceptions import ValidationError
import requests


class WeatherAPIProxyView(APIView):

    def get(self, request, *args, **kwargs):
        lat = request.query_params.get('lat')
        lon = request.query_params.get('lon')

        if not lat and not lon:
            raise ValidationError("brak lat lub lon")

        response = requests.get(
            settings.WETHERAPI_ADDRESS.format(lat=lat, lon=lon)).json()

        result = {
            'name': response.get('name'),
            'rain': bool(response.get('rain')),
        }

        return Response(result, status=200)
